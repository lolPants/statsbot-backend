<h1 align='center'>StatsBot Backend <img src='https://gitlab.com/lolPants/statsbot-backend/badges/master/build.svg'/></h1>
<h3 align='center'>NOTE: Node.js Version 8 or higher is <i>REQUIRED</i></h3>

## About
This framework was created using [Discord.js](https://discord.js.org) as a unifying solution for creating bots using a simple to understand framework. The name `StatsBot Backend` comes from one of my original projects, [StatsBot](https://gitlab.com/lolPants/statsbot-2). Ripping the logic from the bot handlers into a separate package, I can maintain multiple bots with the same backend without having to worry about keeping two bots' core the same.

## Installation
Using NPM: `npm install statsbot-backend`

Using Yarn: `yarn add statsbot-backend`


## Setup
Once installed, import the package:
```js
const Backend = require('statsbot-backend')
```


### MORE DOCUMENTATION COMING SOON PROBABLY