/**
 * @typedef {Object} BuilderProperties
 * @property {string} name
 * @property {string} message
 * @property {boolean} error
 * @property {boolean} pending
 */

// Emoji Constants
const EMOJI = {
  ERROR: ':x:',
  SUCCESS: ':white_check_mark:',
  PENDING: ':hourglass:',
}

/**
 * Builder for status messages
 */
class MessageBuilder {
  /**
   * @param {BuilderProperties} data Defaut Message Builder Properties
   */
  constructor (data = {
    name: '',
    message: '',
    error: false,
    pending: false,
  }) {
    this.setup(data)
  }

  /**
   * Setup Message Builder Properties
   * @param {BuilderProperties} data Defaut Message Builder Properties
   */
  setup (data) {
    /**
     * Module Name
     * @type {?string}
     */
    this.name = data.name || ''

    /**
     * Message Text
     * @type {?string}
     */
    this.message = data.message || ''

    /**
     * Message Error Flag
     * @type {?boolean}
     */
    this.error = data.error || false

    /**
     * Message Pending Flag
     * @type {?boolean}
     */
    this.pending = data.pending || false
  }

  /**
   * Name of the invoking Module. (Optional)
   * @param {string} name Set Module Name
   * @returns {MessageBuilder}
   */
  setName (name) {
    this.name = name
    return this
  }

  /**
   * Main Message String
   * @param {string} message Set Message
   * @returns {MessageBuilder}
   */
  setMessage (message) {
    this.message = message
    return this
  }

  /**
   * Error Flag: Changes status emoji to a Cross
   * @param {boolean} [bool=true] Set Error Flag
   * @returns {MessageBuilder}
   */
  setError (bool = true) {
    this.error = bool
    return this
  }

  /**
   * Pending Flag: Changes status emoji to a Cross
   * @param {boolean} [bool=true] Set Pending Flag
   * @returns {MessageBuilder}
   */
  setPending (bool = true) {
    this.pending = bool
    return this
  }

  /**
   * @returns {string}
   */
  toString () {
    let message = ''

    // Append Emoji
    let emoji = this.error ? EMOJI.ERROR : this.pending ? EMOJI.PENDING : EMOJI.SUCCESS
    message += emoji

    // Append Module Name (if applicable)
    if (this.name !== '') message += ` **${this.name}:**`

    // Append main message
    message += ` ${this.message}`

    return message
  }

  /**
   * @type {string}
   */
  get formattedMessage () {
    return this.toString()
  }

  /**
   * @type {string}
   */
  get formatted () {
    return this.toString()
  }
}

module.exports = MessageBuilder
