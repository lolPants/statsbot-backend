/**
 * @typedef {Object} Inhibitor
 * @property {function} evaluator
 * @property {string} errorMessage
 * @property {boolean} silent
 */

/**
 * @typedef {Object} CommandProperties
 * @property {string} name
 * @property {function} main
 * @property {boolean} admin
 * @property {boolean} owner
 * @property {boolean} nsfw
 * @property {string} description
 * @property {string[]} arguments
 * @property {Inhibitor[]} inhibitors
 */

/**
 * Represents an executable Command
 */
class Command {
  /**
   * @param {CommandProperties} data Default Command Properties
   */
  constructor (data = {
    name: '',
    main: () => {}, // eslint-disable-line
    admin: false,
    owner: false,
    nsfw: false,
    description: '',
    arguments: [],
    inhibitors: [],
  }) {
    this.setup(data)
  }

  /**
   * Setup Command Properties
   * @param {CommandProperties} data Default Command Properties
   */
  setup (data) {
    /**
     * Name of the command. Used to invoke
     * @type {?string}
     */
    this.name = data.name

    /**
     * The function called when this command is invoked
     * @type {?function}
     */
    this.main = data.main

    /**
     * Admin Flag
     * @type {?boolean}
     */
    this.admin = data.admin

    /**
     * Owner Flag
     * @type {?boolean}
     */
    this.owner = data.owner

    /**
     * NSFW Flag
     * @type {?boolean}
     */
    this.nsfw = data.nsfw

    /**
     * A brief description of the commands functionality
     * @type {?string}
     */
    this.description = data.description

    /**
     * Ordered command arguments (only used in help commands)
     * @type {?string[]}
     */
    this.arguments = data.arguments

    /**
     * Ordered command arguments (only used in help commands)
     * @type {?Inhibitor[]}
     */
    this.inhibitors = data.inhibitors
  }

  /**
   * Name of the command. Used to invoke
   * @param {string} name Set Command Name
   * @returns {Command}
   */
  setName (name) {
    this.name = name.toLowerCase()
    return this
  }

  /**
   * The function called when this command is invoked
   * @param {Function} func Set Main Function
   * @returns {Command}
   */
  setMain (func) {
    this.main = func
    return this
  }

  /**
   * Admin Flag: Users must have the ADMINISTRATOR permission on a server, or be the configured Bot Owner
   * @param {boolean} [bool=true] Set Admin Flag
   * @returns {Command}
   */
  setAdmin (bool = true) {
    this.admin = bool
    return this
  }

  /**
   * Owner Flag: Command can only be used by the configured Bot Owner
   * @param {boolean} [bool=true] Set Owner Flag
   * @returns {Command}
   */
  setOwner (bool = true) {
    this.owner = bool
    return this
  }

  /**
   * NSFW Flag: Command can only be used in NSFW Channels
   * @param {boolean} [bool=true] Set NSFW Flag
   * @returns {Command}
   */
  setNSFW (bool = true) {
    this.nsfw = bool
    return this
  }

  /**
   * A brief description of the commands functionality
   * @param {string} str Set Command Description
   * @returns {Command}
   */
  setDescription (str) {
    this.description = str
    return this
  }

  /**
   * Ordered command arguments (only used in help commands)
   * @param {string[]} args Set Command Arguments
   * @returns {Command}
   */
  setArguments (args) {
    this.arguments = args
    return this
  }

  /**
   * Ordered command arguments (only used in help commands)
   * @param {string[]|string} args Add a Command Argument
   * @returns {Command}
   */
  addArgument (args) {
    if (Array.isArray(args)) this.arguments = [...this.arguments, ...args]
    else if (typeof args === 'string') this.arguments = [...this.arguments, args]
    return this
  }

  /**
   * Ordered Inhibitors. Executed in order, if any return truthy values the command function will not execute.
   * @param {Inhibitor[]} inhibitor Set Inhibitors
   * @returns {Command}
   */
  setInhibitors (inhibitor) {
    this.inhibitors = inhibitor
    return this
  }

  /**
   * Ordered Inhibitors. Executed in order, if any return truthy values the command function will not execute.
   * @param {Inhibitor[]|Inhibitor} inhibitor Add an Inhibitor
   * @returns {Command}
   */
  addInhibitor (inhibitor) {
    if (Array.isArray(inhibitor)) this.inhibitors = [...this.inhibitors, ...inhibitor]
    else if (typeof inhibitor === 'object') this.inhibitors = [...this.inhibitors, inhibitor]
    return this
  }
}

module.exports = Command
