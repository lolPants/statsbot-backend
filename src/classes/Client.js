const Discord = require('discord.js')
const log = require('fancylog')

// Handler Helpers
const handlers = require('../handlers')
const Registry = require('./Registry')

// JSDoc Typing
const Types = require('../index')
const Client = Discord.Client
const RegistryType = Types.Registry // eslint-disable-line
const Command = Types.Command // eslint-disable-line
const DiscordMessage = Discord.Message // eslint-disable-line

/**
 * @typedef {Object} Status
 * @property {string} text
 * @property {string} [type]
 */

/**
 * @typedef {Object} BackendClientOptions
 * @property {string} prefix
 * @property {boolean} ignorePrefixSpacing
 * @property {string} name
 * @property {string} description
 * @property {string} author
 * @property {string|string[]} owners
 * @property {Status|Status[]} statuses
 * @property {string|string[]} bannedGuilds
 * @property {string} statusURL
 * @property {RegistryType|RegistryType[]} registries
 * @property {boolean} announceInvalid
 * @property {Buffer} avatar
 */

/**
 * @typedef {Object} CommandProperties
 * @property {BackendClient} client
 * @property {DiscordMessage} message
 * @property {Command} command
 * @property {string[]} arguments
 */

/**
 * Discord.js Client with StatsBot Backend Framework hooked in
 * @extends {Client}
 */
class BackendClient extends Client {
  /**
   * @param {BackendClientOptions} data Backend Client Options
   * @param {ClientOptions} options Discord Client Options
   */
  constructor (data = {}, options = undefined) {
    super(options)
    this.setup(data)

    this.on('ready', async () => {
      log.i('Connected to Discord')

      // Check for Banned Servers
      this._checkBannedGuilds()

      // Add Application Owner to list
      this.fetchApplication()
        .then(app => { this.addOwner(app.owner.id) })

      // Status Loop
      const setGame = () => {
        if (this.statuses.length === 0) return

        let status = this.statuses[Math.floor(Math.random() * this.statuses.length)]
        if (this.statusURL === undefined || this.statusURL === null || this.statusURL === '') {
          let type = status.type.toUpperCase()
          if (['PLAYING', 'LISTENING', 'WATCHING'].includes(type)) {
            this.user.setActivity(status.text, { type })
          } else {
            this.user.setActivity(status.text)
          }
        } else {
          this.user.setActivity(status.text, { url: this.statusURL, type: 'STREAMING' })
        }
      }
      setGame()
      setInterval(() => { setGame() }, 15 * 1000)

      // Set Avatar
      if (this.avatar !== undefined) {
        // Avatar Defined
        try {
          await this.user.setAvatar(this.avatar)
        } catch (err) {
          // No-op
        }
      }
    })

    this.on('message', message => {
      // Ignore messages from bots
      if (message.author.bot) return

      // Check for Banned Servers
      this._checkBannedGuilds()

      let commandTest = handlers.prefixHandler(this.prefix, message.content, this.ignorePrefixSpacing)
      if (commandTest !== false) {
        // Input is formatted like a command
        let command = this.mergedRegistry.findCommand(commandTest.name)
        if (command !== null) {
          // Command Exists
          this._handleCommand(command, message)
        } else if (this.announceInvalid) {
          // Command Doesn't Exist
          message.channel.send(':x: Command Doesn\'t Exist.')
        }
      }
    })

    this.on('guildCreate', () => {
      // Check for Banned Servers
      this._checkBannedGuilds()
    })
  }

  /**
   * Check a list of banned servers
   * @private
   */
  _checkBannedGuilds () {
    // Map all banned servers to a promise array
    let queue = this.bannedGuilds.map(async ID => {
      // Get the guild for an ID
      let guild = await this.guilds.get(ID)

      // Check if the bot is in the guild
      if (guild === undefined) {
        // If not, do nothing
        return null
      } else {
        // If so, grab the default channel
        let channel = await guild.systemChannel
        try {
          // Attempt to send a message and leave
          await channel.send(`:x: This server is banned from using ${this.name}`)
          return guild.leave()
        } catch (err) {
          // If the message cannot be send (missing perms), just leave anyway
          return guild.leave()
        }
      }
    })

    // Execute all leave requests
    Promise.all(queue)
  }

  /**
   * Handle a Command to Execute
   * @param {Command} command Command Object
   * @param {DiscordMessage} message Discord.js Message Object
   * @private
   */
  _handleCommand (command, message) {
    if (command.nsfw) {
      // NSFW Flag
      if (message.channel.nsfw) this._checkInhibitors(command, message)
      else message.channel.send(':x: NSFW commands can only be used in NSFW Channels.')
    } else if (command.owner) {
      // Owner Flag
      if (this.owners.filter(id => id === message.author.id).length > 0) this._checkInhibitors(command, message)
      else message.channel.send(':x: You do not have permission to do that.')
    } else if (command.admin) {
      // Admin Flag
      let isOwner = this.owners.filter(id => id === message.author.id).length > 0
      let isAdmin = message.guild === null ? false : message.member.hasPermission('ADMINISTRATOR')

      if (isOwner || isAdmin) this._checkInhibitors(command, message)
      else message.channel.send(':x: You do not have permission to do that.')
    } else {
      this._checkInhibitors(command, message)
    }
  }

  /**
   * Check a command for inhibitors
   * @param {Command} command Command Object
   * @param {DiscordMessage} message Discord.js Message Object
   * @private
   */
  async _checkInhibitors (command, message) {
    let commandTest = handlers.prefixHandler(this.prefix, message.content, this.ignorePrefixSpacing)
    let args = handlers.argumentSplitter(commandTest.arguments)

    /**
     * @type {CommandProperties}
     */
    let commandProperties = {
      client: this,
      message,
      command,
      arguments: args,
    }

    if (command.inhibitors.length > 0) {
      // If the command has inhibitors
      let i = 0
      for (let inhibitor of command.inhibitors) {
        let shouldInhibit = await inhibitor.evaluator(commandProperties) // eslint-disable-line
        if (shouldInhibit && inhibitor.silent) {
          // No Op
          break
        } else if (shouldInhibit && !inhibitor.silent) {
          // Inhibit with message
          message.channel.send(inhibitor.errorMessage)
          break
        } else {
          // No Inhibitors
          i++
        }
        if (i === command.inhibitors.length) command.main(commandProperties)
      }
    } else {
      // If no inhibitors, skip checking
      command.main(commandProperties)
    }
  }

  /**
   * Setup Class Variables
   * @param {BackendClientOptions} data Client Options
   */
  setup (data) {
    /**
     * Command Prefix
     * @type {?string}
     */
    this.prefix = data.prefix

    /**
     * Command Prefix - Ignore Spacing
     * @type {?boolean}
     */
    this.ignorePrefixSpacing = data.ignorePrefixSpacing || false

    /**
     * Bot Name
     * @type {?string}
     */
    this.name = data.name

    /**
     * Bot Description
     * @type {?string}
     */
    this.description = data.description

    /**
     * Bot Author
     * @type {?string}
     */
    this.author = data.author

    /**
     * Array of Owner IDs
     * @type {?string[]}
     */
    this.owners = typeof data.owners === 'string' ? [data.owners] : Array.isArray(data.owners) ? [...data.owners] : []

    /**
     * Array of Statuses
     * @type {?Status[]}
     */
    this.statuses = typeof data.statuses === 'object' ?
      [data.statuses] : Array.isArray(data.statuses) ? [...data.statuses] : []

    /**
     * Array of Banned Guilds
     * @type {?string[]}
     */
    this.bannedGuilds = typeof data.bannedGuilds === 'string' ?
      [data.bannedGuilds] : Array.isArray(data.bannedGuilds) ? [...data.bannedGuilds] : []

    /**
     * Status Streaming URL
     * @type {string}
     */
    this.statusURL = data.statusURL

    /**
     * Array of Command Registries
     * @type {?RegistryType[]}
     */
    this.registries = typeof data.registries === 'object' ?
      [data.registries] : Array.isArray(data.registries) ? [...data.registries] : []

    /**
     * Announce if the command is invalid
     * @type {?boolean}
     */
    this.announceInvalid = data.announceInvalid || false

    /**
     * Bot Avatar
     * @type {?Buffer}
     */
    this.avatar = data.avatar
  }

  /**
   * Set Bot Prefix for Command Handling
   * @param {string} prefix Bot Prefix
   * @returns {BackendClient}
   */
  setPrefix (prefix) {
    this.prefix = prefix
    return this
  }

  /**
   * Ignore Command Prefix Spacing Flag
   * @param {boolean} [bool] Flag Status
   * @returns {BackendClient}
   */
  setIgnorePrefixSpacing (bool = true) {
    this.ignorePrefixSpacing = bool
    return this
  }

  /**
   * Set Bot Name. Used in Help Commands
   * @param {string} name Bot Name
   * @returns {BackendClient}
   */
  setName (name) {
    this.name = name
    return this
  }

  /**
   * Set Bot Description. Used in Help Commands
   * @param {string} description Bot Description
   * @returns {BackendClient}
   */
  setDescription (description) {
    this.description = description
    return this
  }

  /**
   * Set Bot Author. Used in Help Commands
   * @param {string} author Bot Author
   * @returns {BackendClient}
   */
  setAuthor (author) {
    this.author = author
    return this
  }

  /**
   * Set Bot Owners: Array of Stringified Discord IDs
   * @param {string[]} owners Owner ID Array
   * @returns {BackendClient}
   */
  setOwners (owners) {
    this.owners = owners
    return this
  }

  /**
   * Add Bot Owners: Stringified Discord IDs
   * @param {string|string[]} owner Owner IDs
   * @returns {BackendClient}
   */
  addOwner (owner) {
    if (Array.isArray(owner)) this.owners = [...this.owners, ...owner]
    else this.owners = [...this.owners, owner]
    return this
  }

  /**
   * Set Bot Statuses
   * @param {Status[]} statuses Bot Status Array
   * @returns {BackendClient}
   */
  setStatuses (statuses) {
    statuses = statuses.map(x => typeof x === 'string' ? { text: x } : x)
    this.statuses = statuses

    // Map Strings to Objects
    this.statuses = this.statuses.map(x => typeof x === 'string' ? { text: x } : x)

    return this
  }

  /**
   * Add Bot Statuses
   * @param {Status|Status[]} status Bot Statuses
   * @returns {BackendClient}
   */
  addStatus (status) {
    if (Array.isArray(status)) this.statuses = [...this.statuses, ...status]
    else this.statuses = [...this.statuses, status]

    // Map Strings to Objects
    this.statuses = this.statuses.map(x => typeof x === 'string' ? { text: x } : x)

    return this
  }

  /**
   * Set Banned Guild IDs
   * @param {string[]} serverIDs Bot Banned Server ID List
   * @returns {BackendClient}
   */
  setBannedGuilds (serverIDs) {
    this.bannedGuilds = serverIDs
    return this
  }

  /**
   * Add a Banned Guild ID
   * @param {string|string[]} serverID Bot Banned Server ID
   * @returns {BackendClient}
   */
  addBannedGuild (serverID) {
    if (Array.isArray(serverID)) this.bannedGuilds = [...this.bannedGuilds, ...serverID]
    else this.bannedGuilds = [...this.bannedGuilds, serverID]
    return this
  }

  /**
   * Set Status Streaming URL
   * @param {string} url Status URL
   * @returns {BackendClient}
   */
  setStatusURL (url) {
    this.statusURL = url
    return this
  }

  /**
   * Set Command Registries
   * @param {RegistryType[]} registries Command Registries Array
   * @returns {BackendClient}
   */
  setRegistries (registries) {
    this.registries = registries
    return this
  }

  /**
   * Add Command Registry
   * @param {RegistryType|RegistryType[]} registry Command Registry
   * @returns {BackendClient}
   */
  addRegistry (registry) {
    if (Array.isArray(registry)) this.registries = [...this.registries, ...registry]
    else this.registries = [...this.registries, registry]
    return this
  }

  get mergedRegistry () {
    let registry = new Registry()
    for (let reg of this.registries) {
      for (let command of reg.commands) {
        registry.addCommand(command)
      }
    }
    return registry
  }

  /**
   * Whether to annouce if a command is invalid (Default = false)
   * @param {boolean} [bool] Flag Status
   * @returns {BackendClient}
   */
  setAnnounceInvalid (bool = true) {
    this.announceInvalid = bool
    return this
  }

  /**
   * Set Bot Avatar
   * @param {Buffer} buffer Bot Avatar
   * @returns {BackendClient}
   */
  setAvatar (buffer) {
    this.avatar = buffer
    return this
  }

  /**
   * @type {boolean}
   */
  get isSelfbot () { return false }
}

module.exports = BackendClient
