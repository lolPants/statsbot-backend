// JSDoc Typing
const Types = require('../index')
const Command = Types.Command // eslint-disable-line

/**
 * @typedef {Object} RegistryProperties
 * @property {Command[]} commands
 */

class Registry {
  /**
   * @param {RegistryProperties} data Default Command Registry Properties
   */
  constructor (data = { commands: [] }) {
    this.setup(data)
  }

  /**
   * Setup Registry Properties
   * @param {RegistryProperties} data Default Command Registry Properties
   */
  setup (data) {
    /**
     * @type {?Command[]}
     */
    this._commands = data.commands
  }

  get commands () {
    return this._commands.sort((a, b) => a.name > b.name ? 1 : b.name > a.name ? -1 : 0)
  }

  /**
   * Add a Command to the current registry
   * @param {Command|Command[]} command Command Object
   * @returns {Registry}
   */
  addCommand (command) {
    if (Array.isArray(command)) this._commands = [...this._commands, ...command]
    else this._commands = [...this._commands, command]
    return this
  }

  /**
   * Find a command based on its name
   * @param {string} name Command Name
   * @returns {Command}
   */
  findCommand (name) {
    try {
      let arr = this._commands.filter(c => c.name === name.toLowerCase())
      if (arr.length === 0) return null
      else return arr[0]
    } catch (err) {
      return null
    }
  }
}

module.exports = Registry
