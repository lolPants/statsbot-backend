const Discord = require('discord.js')
const log = require('fancylog')

// Handler Helpers
const handlers = require('../handlers')
const Registry = require('./Registry')

// JSDoc Typing
const Types = require('../index')
const Client = Discord.Client
const RegistryType = Types.Registry // eslint-disable-line
const Command = Types.Command // eslint-disable-line
const DiscordMessage = Discord.Message // eslint-disable-line

/**
 * @typedef {Object} SelfbotClientOptions
 * @property {string} prefix
 * @property {boolean} ignorePrefixSpacing
 * @property {string} name
 * @property {string} description
 * @property {string} author
 * @property {number} timeout
 * @property {string[]} ignoredGuilds
 * @property {RegistryType|RegistryType[]} registries
 */

/**
 * @typedef {Object} CommandProperties
 * @property {SelfbotClient} client
 * @property {DiscordMessage} message
 * @property {Command} command
 * @property {string[]} arguments
 */

/**
 * Discord.js Client with StatsBot Backend Framework hooked in for Selfbots
 * @extends {Client}
 */
class SelfbotClient extends Client {
  /**
   * @param {SelfbotClientOptions} data Backend Client Options
   * @param {ClientOptions} options Discord Client Options
   */
  constructor (data = {}, options = undefined) {
    super(options)
    this.setup(data)

    this.on('ready', () => {
      log.i('Connected to Discord')
    })

    this.on('message', message => {
      // Ignore messages from bots
      if (message.author !== this.user) return

      // Check for ignored guilds
      if (this.ignoredGuilds.some(x => x === message.guild.id)) return

      let commandTest = handlers.prefixHandler(this.prefix, message.content, this.ignorePrefixSpacing)
      if (commandTest !== false) {
        // Input is formatted like a command
        let command = this.mergedRegistry.findCommand(commandTest.name)
        if (command !== null) {
          // Command Exists
          this._handleCommand(command, message)
        } else {
          // Command Doesn't Exist
          this._sendErrorMessage(message, ':x: Command Doesn\'t Exist.')
        }
      }
    })
  }

  /**
   * Handle a Command to Execute
   * @param {Command} command Command Object
   * @param {DiscordMessage} message Discord.js Message Object
   * @private
   */
  _handleCommand (command, message) {
    if (command.nsfw) {
      // NSFW Flag
      if (message.channel.nsfw) this._checkInhibitors(command, message)
      else this._sendErrorMessage(message, ':x: NSFW commands can only be used in NSFW Channels.')
    } else {
      this._checkInhibitors(command, message)
    }
  }

  /**
   * Check a command for inhibitors
   * @param {Command} command Command Object
   * @param {DiscordMessage} message Discord.js Message Object
   * @private
   */
  async _checkInhibitors (command, message) {
    let commandTest = handlers.prefixHandler(this.prefix, message.content, this.ignorePrefixSpacing)
    let args = handlers.argumentSplitter(commandTest.arguments)

    /**
     * @type {CommandProperties}
     */
    let commandProperties = {
      client: this,
      message,
      command,
      arguments: args,
    }

    if (command.inhibitors.length > 0) {
      // If the command has inhibitors
      let i = 0
      for (let inhibitor of command.inhibitors) {
        let shouldInhibit = await inhibitor.evaluator(commandProperties) // eslint-disable-line
        if (shouldInhibit && inhibitor.silent) {
          // No Op
          break
        } else if (shouldInhibit && !inhibitor.silent) {
          // Inhibit with message
          this._sendErrorMessage(message, inhibitor.errorMessage)
          break
        } else {
          // No Inhibitors
          i++
        }
        if (i === command.inhibitors.length) command.main(commandProperties)
      }
    } else {
      // If no inhibitors, skip checking
      command.main(commandProperties)
    }
  }

  /**
   * Sends a Temporary Error Message
   * @param {DiscordMessage} message Message Object
   * @param {string} str Message String
   * @private
   */
  async _sendErrorMessage (message, str) {
    let msg = await message.edit(str)
    setTimeout(() => { msg.delete() }, this.timeout)
  }

  /**
   * Setup Class Variables
   * @param {SelfbotClientOptions} data Client Options
   */
  setup (data) {
    /**
     * Command Prefix
     * @type {?string}
     */
    this.prefix = data.prefix

    /**
     * Command Prefix - Ignore Spacing
     * @type {?boolean}
     */
    this.ignorePrefixSpacing = data.ignorePrefixSpacing || false

    /**
     * Bot Name
     * @type {?string}
     */
    this.name = data.name

    /**
     * Bot Description
     * @type {?string}
     */
    this.description = data.description

    /**
     * Bot Author
     * @type {?string}
     */
    this.author = data.author

    /**
     * Message Timeout Delay (ms)
     * @type {?number}
     */
    this.timeout = data.timeout || 5000

    /**
     * Array of ignored Guilds
     * @type {?string[]}
     */
    this.ignoredGuilds = typeof data.ignoredGuilds === 'object' ?
      [data.ignoredGuilds] : Array.isArray(data.ignoredGuilds) ? [...data.ignoredGuilds] : []

    /**
     * Array of Command Registries
     * @type {?RegistryType[]}
     */
    this.registries = typeof data.registries === 'object' ?
      [data.registries] : Array.isArray(data.registries) ? [...data.registries] : []
  }

  /**
   * Set Bot Prefix for Command Handling
   * @param {string} prefix Bot Prefix
   * @returns {SelfbotClient}
   */
  setPrefix (prefix) {
    this.prefix = prefix
    return this
  }

  /**
   * Ignore Command Prefix Spacing Flag
   * @param {boolean} [bool] Flag Status
   * @returns {SelfbotClient}
   */
  setIgnorePrefixSpacing (bool = true) {
    this.ignorePrefixSpacing = bool
    return this
  }

  /**
   * Set Bot Name. Used in Help Commands
   * @param {string} name Bot Name
   * @returns {SelfbotClient}
   */
  setName (name) {
    this.name = name
    return this
  }

  /**
   * Set Bot Description. Used in Help Commands
   * @param {string} description Bot Description
   * @returns {SelfbotClient}
   */
  setDescription (description) {
    this.description = description
    return this
  }

  /**
   * Set Bot Author. Used in Help Commands
   * @param {string} author Bot Author
   * @returns {SelfbotClient}
   */
  setAuthor (author) {
    this.author = author
    return this
  }

  /**
   * Set Bot Message Timeout
   * @param {number} ms Timeout (ms)
   * @returns {SelfbotClient}
   */
  setMessageTimeout (ms) {
    if (Number.isNaN(ms)) throw new Error('Timeout must be an integer')
    this.timeout = parseInt(ms) || 5000
    return this
  }

  /**
   * Set Ignored Guild IDs
   * @param {string[]} serverIDs Bot Banned Server ID List
   * @returns {SelfbotClient}
   */
  setIgnoredGuilds (serverIDs) {
    this.ignoredGuilds = serverIDs
    return this
  }

  /**
   * Add an Ignored Guild ID
   * @param {string|string[]} serverID Bot Banned Server ID
   * @returns {SelfbotClient}
   */
  addIgnoredGuild (serverID) {
    if (Array.isArray(serverID)) this.ignoredGuilds = [...this.ignoredGuilds, ...serverID]
    else this.ignoredGuilds = [...this.ignoredGuilds, serverID]
    return this
  }

  /**
   * Set Command Registries
   * @param {RegistryType[]} registries Command Registries Array
   * @returns {SelfbotClient}
   */
  setRegistries (registries) {
    this.registries = registries
    return this
  }

  /**
   * Add Command Registry
   * @param {RegistryType|RegistryType[]} registry Command Registry
   * @returns {SelfbotClient}
   */
  addRegistry (registry) {
    if (Array.isArray(registry)) this.registries = [...this.registries, ...registry]
    else this.registries = [...this.registries, registry]
    return this
  }

  get mergedRegistry () {
    let registry = new Registry()
    for (let reg of this.registries) {
      for (let command of reg.commands) {
        registry.addCommand(command)
      }
    }
    return registry
  }

  /**
   * @type {boolean}
   */
  get isSelfbot () { return true }
}

module.exports = SelfbotClient
