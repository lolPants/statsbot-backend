/**
 * @typedef {Object} InhibitorProperties
 * @property {function} evaluator
 * @property {string} errorMessage
 */

/**
 * Represents a Command Inhibitor
 */
class Inhibitor {
  /**
   * @param {InhibitorProperties} data Default Inhibitor Properties
   */
  constructor (data = {
    evaluator: () => false,
    errorMessage: '',
  }) {
    this.setup(data)
  }

  /**
   * Setup Inhibitor Properties
   * @param {InhibitorProperties} data Default Inhibitor Properties
   */
  setup (data) {
    /**
     * Evaluator Function. Inhibits command execution if this function returns a truthy value
     * @type {?function}
     */
    this.evaluator = data.evaluator

    /**
     * Custom Error Message. If set, it will be sent to the channel if a command is inhibited
     * @type {?string}
     */
    this.errorMessage = data.errorMessage
  }

  /**
   * Whether this inhibitor should silently execute
   * @returns {boolean}
   */
  get silent () {
    if (
      this.errorMessage === '' ||
      this.errorMessage === null ||
      this.errorMessage === undefined
    ) return true
    else return false
  }

  /**
   * Set Evaluator Function. Inhibits command execution if this function returns a truthy value
   * @param {Function} func Evaluator Function
   * @returns {Inhibitor}
   */
  setEvaluator (func) {
    this.evaluator = func
    return this
  }

  /**
   * Set a Custom Error Message. If set, it will be sent to the channel if a command is inhibited
   * @param {string} string Error Message
   * @returns {Inhibitor}
   */
  setErrorMessage (string) {
    this.errorMessage = string
    return this
  }
}

module.exports = Inhibitor
