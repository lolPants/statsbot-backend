// Classes
const Client = require('./classes/Client')
const SelfbotClient = require('./classes/SelfbotClient')
const Command = require('./classes/Command')
const Inhibitor = require('./classes/Inhibitor')
const Registry = require('./classes/Registry')
const MessageBuilder = require('./classes/MessageBuilder')

// Standard Registry / Commands
const { StandardRegistry, SelfbotRegistry } = require('./commands/StandardRegistry')
const Name = require('./commands/name')
const Nick = require('./commands/nick')
const ChannelHelp = require('./commands/channelHelp')
const DynamicHelp = require('./commands/dynamicHelp')

// Standard Inhibitors
const StandardInhibitors = require('./inhibitors/StandardInhibitors')

module.exports = {
  Client,
  SelfbotClient,
  Command,
  Inhibitor,
  Registry,
  MessageBuilder,
  StandardRegistry,
  SelfbotRegistry,
  StandardInhibitors,
  StandardCommands: {
    Name,
    Nick,
    ChannelHelp,
    DynamicHelp,
  },
}
