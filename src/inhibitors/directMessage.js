const Inhibitor = require('../classes/Inhibitor')
let MessageBuilder = require('../classes/MessageBuilder')

let DirectMessage = new Inhibitor()
  .setErrorMessage(
    new MessageBuilder()
      .setError()
      .setMessage('This command cannot be used in Direct Messages!')
      .formattedMessage
  )
  .setEvaluator(data => data.message.guild === null)

module.exports = DirectMessage
