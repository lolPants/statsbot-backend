const Command = require('../classes/Command')
const Backend = require('../index')
const helpCommand = require('./help')

// JSDoc Typing
const Discord = require('discord.js')
const BackendClient = Backend.Client // eslint-disable-line
const DiscordMessage = Discord.Message // eslint-disable-line

/**
 * @typedef {Object} CommandProperties
 * @property {BackendClient} client
 * @property {DiscordMessage} message
 * @property {Command} command
 * @property {string[]} arguments
 */

/**
 * @param {CommandProperties} data Command Properties
 */
const main = async data => {
  let embed = await helpCommand(data)
  data.message.channel.send('', { embed })
}

const Help = new Command()
  .setName('help')
  .setDescription('Lists all commands. You\'re using it right now!')
  .setMain(main)

module.exports = Help
