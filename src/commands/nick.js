const Command = require('../classes/Command')
const MessageBuilder = require('../classes/MessageBuilder')
const StandardInhibitors = require('../inhibitors/StandardInhibitors')
const Backend = require('../index')

// JSDoc Typing
const Discord = require('discord.js')
const BackendClient = Backend.Client // eslint-disable-line
const DiscordMessage = Discord.Message // eslint-disable-line

/**
 * @typedef {Object} CommandProperties
 * @property {BackendClient} client
 * @property {DiscordMessage} message
 * @property {Command} command
 * @property {string[]} arguments
 */

/**
 * @param {CommandProperties} data Command Properties
 */
const main = data => {
  if (data.arguments.length === 0) setNickname(data)
  else setNickname(data, data.arguments.join(' '))
}

/**
 * Sets the bot's nickname
 * @param {CommandProperties} data Command Properties
 * @param {string} nickname Nickname to set to
 */
const setNickname = async (data, nickname = null) => {
  let message = new MessageBuilder()
    .setName('Nickname')
    .setError()

  // Check if bot has perms to change own nickname
  let member = await data.message.guild.members.get(data.client.user.id)
  // If they do have perms:
  if (member.hasPermission('CHANGE_NICKNAME') || member.hasPermission('MANAGE_NICKNAMES')) {
    try {
      // Try to set nickname
      await member.setNickname(nickname)

      // If nickname is null, then we reset. Send according messages
      if (nickname === null) data.message.channel.send(message.setError(false).setMessage('Nickname Reset!').formattedMessage)
      else data.message.channel.send(message.setError(false).setMessage(`Nickname changed to **${nickname}**.`).formattedMessage)
    } catch (err) {
      // Catch errors
      data.message.channel.send(message.setMessage('Error changing nickname! Try again...').formattedMessage)
    }
  } else {
    // If no perms, send error message
    data.message.channel.send(message.setMessage('I don\'t have permission to do that!').formattedMessage)
  }
}

const Nickname = new Command()
  .setName('nick')
  .setAdmin()
  .addInhibitor(StandardInhibitors.DirectMessage)
  .setDescription('Changes the Bot\'s Nickname')
  .addArgument('?nick')
  .setMain(main)

module.exports = Nickname
