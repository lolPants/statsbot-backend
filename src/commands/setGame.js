const Command = require('../classes/Command')
const Backend = require('../index')
const MessageBuilder = require('../classes/MessageBuilder')

// JSDoc Typing
const Discord = require('discord.js')
const SelfbotClient = Backend.SelfbotClient // eslint-disable-line
const DiscordMessage = Discord.Message // eslint-disable-line

/**
 * @typedef {Object} CommandProperties
 * @property {SelfbotClient} client
 * @property {DiscordMessage} message
 * @property {Command} command
 * @property {string[]} arguments
 */

/**
 * @param {CommandProperties} data Command Properties
 */
const main = async data => {
  // Message Builder
  let msgString = new MessageBuilder()
    .setName('Set Game')

  try {
    if (data.arguments.length === 0) {
      // No Game Given
      await data.client.user.setActivity(undefined)
      data.client._sendErrorMessage(data.message, msgString.setMessage(
        'Reset Successfully!'
      ).formattedMessage)
    } else {
      let game = data.arguments.join(' ')
      await data.client.user.setActivity(game)
      data.client._sendErrorMessage(data.message, msgString.setMessage(
        `Set to \`${game}\`!`
      ).formattedMessage)
    }
  } catch (err) {
    data.client._sendErrorMessage(data.message, msgString.setMessage(
      'Error setting status!'
    ).setError().formattedMessage)
  }
}

const SetGame = new Command()
  .setName('setgame')
  .setDescription('Sets your *\'Playing\'* status.')
  .addArgument('?status')
  .setMain(main)

module.exports = SetGame
