const Command = require('../classes/Command')
const Backend = require('../index')
const MessageBuilder = require('../classes/MessageBuilder')
const helpCommand = require('./help')

// JSDoc Typing
const Discord = require('discord.js')
const BackendClient = Backend.Client // eslint-disable-line
const SelfbotClient = Backend.SelfbotClient // eslint-disable-line
const DiscordMessage = Discord.Message // eslint-disable-line

/**
 * @typedef {Object} CommandProperties
 * @property {BackendClient|SelfbotClient} client
 * @property {DiscordMessage} message
 * @property {Command} command
 * @property {string[]} arguments
 */

/**
 * @param {CommandProperties} data Command Properties
 */
const main = async data => {
  let embed = await helpCommand(data)
  if (data.client.isSelfbot) {
    let msg = await data.message.edit('', { embed })
    setTimeout(() => { msg.delete() }, data.client.timeout * 2)
  } else if (data.message.guild === null) {
    // Sent in a DM
    data.message.channel.send('', { embed })
  } else {
    try {
      // Send the embed to the user
      await data.message.author.send('', { embed })

      // Make a message to the channel saying the help has been sent
      const sentMessage = new MessageBuilder()
        .setMessage('Help Sent! *(Check your DMs)*')
        .formattedMessage

      // Send confirmation message
      await data.message.channel.send(sentMessage)
    } catch (err) {
      console.error(err)
      // If it fails, and the error is no perms
      // Send directly to the channel
      if (err.code === 50007) data.message.channel.send('', { embed })
    }
  }
}

const Help = new Command()
  .setName('help')
  .setDescription('Lists all commands. You\'re using it right now!')
  .setMain(main)

module.exports = Help
