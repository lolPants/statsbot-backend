// eslint-disable-rule
const Backend = require('../index')

// JSDoc Typing
const Discord = require('discord.js')
const BackendClient = Backend.Client // eslint-disable-line
const DiscordMessage = Discord.Message // eslint-disable-line
const DiscordEmbed = Discord.MessageEmbed // eslint-disable-line

/**
 * @typedef {Object} CommandProperties
 * @property {BackendClient} client
 * @property {DiscordMessage} message
 * @property {Command} command
 * @property {string[]} arguments
 */

/**
 * @param {CommandProperties} data Command Properties
 * @returns {DiscordEmbed}
 */
const Help = async data => {
  // Fetch all commands
  const commands = data.client.mergedRegistry.commands

  // Define and sort commands into categories
  let nsfw = [], owner = [], admin = [], other = []
  for (let command of commands) {
    if (command.nsfw) nsfw.push(command)
    else if (command.owner) owner.push(command)
    else if (command.admin) admin.push(command)
    else other.push(command)
  }

  // Grab Role Colour (if guild)
  let self = data.message.guild === null ? null : await data.message.guild.members.get(data.client.user.id)
  let colour = self !== null && self.displayColor

  // Define embed
  let embed = new Discord.MessageEmbed()
    .setColor(colour)
    .addField(data.client.name || 'Discord Bot', `*${data.client.description || 'Created using StatsBot Backend. Set the Name, Author and Description to edit help data.'}*`) // eslint-disable-line
    .setFooter(`Built by ${data.client.author || 'Somebody'}`)

  // Add command fields (if any commands exist in the categories)
  if (other.length > 0) embed.addField('Commands: These can be used anywhere.', other.map(cmd => commandMap(cmd, data.client)))
  if (nsfw.length > 0) embed.addField('NSFW Commands: These can only be used in NSFW channels.', nsfw.map(cmd => commandMap(cmd, data.client)))
  if (admin.length > 0) embed.addField('Admin Commands: These can only be used by the bot owner, or users with the ADMINISTRATOR permission.', admin.map(cmd => commandMap(cmd, data.client))) // eslint-disable-line
  if (owner.length > 0) embed.addField('Owner Commands: These can only be used by the bot owner.', owner.map(cmd => commandMap(cmd, data.client)))

  return embed
}

/**
 * @param {Command} command Command Object
 * @param {BackendClient} client Client Object
 * @returns {string}
 */
const commandMap = (command, client) => {
  let out = ''
  // List Command Name
  if (client.ignorePrefixSpacing) {
    out += `\`${client.prefix}${command.name}\` `
  } else {
    out += `\`${client.prefix}\` `
    out += `\`${command.name}\` `
  }

  // List Arguments (if available)
  if (command.arguments !== undefined && command.arguments.length > 0) {
    for (let arg of command.arguments) { out += `\`${arg}\` ` }
  }

  // List Description (if available)
  if (command.description !== undefined && command.description !== '') out += `- ${command.description}`
  return out
}

module.exports = Help
