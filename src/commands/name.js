const Command = require('../classes/Command')
const MessageBuilder = require('../classes/MessageBuilder')
const Backend = require('../index')

// JSDoc Typing
const Discord = require('discord.js')
const BackendClient = Backend.Client // eslint-disable-line
const DiscordMessage = Discord.Message // eslint-disable-line

/**
 * @typedef {Object} CommandProperties
 * @property {BackendClient} client
 * @property {DiscordMessage} message
 * @property {Command} command
 * @property {string[]} arguments
 */

/**
 * @param {CommandProperties} data Command Properties
 */
const main = async data => {
  if (data.arguments.length === 0) {
    const noArgs = new MessageBuilder()
      .setName('Name')
      .setMessage('You didn\'t specify a name!')
      .setError()
      .formattedMessage

    data.message.channel.send(noArgs)
  } else {
    let username = data.arguments.join(' ')

    let errorMessage = new MessageBuilder()
      .setName('Name')
      .setMessage('Error changing name! Try again...')
      .setError()
      .formattedMessage

    let changedMessage = new MessageBuilder()
      .setName('Name')
      .setMessage(`Name changed to **${username}**.`)
      .formattedMessage

    try {
      await data.client.user.setUsername(username)
      data.message.channel.send(changedMessage)
    } catch (err) {
      data.message.channel.send(errorMessage)
    }
  }
}

const NameChange = new Command()
  .setName('name')
  .setOwner()
  .setDescription('Changes the Bot\'s Username')
  .addArgument('name')
  .setMain(main)

module.exports = NameChange
