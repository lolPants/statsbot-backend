const Registry = require('../classes/Registry')

const Nickname = require('../commands/nick')
const NameChange = require('../commands/name')
const Help = require('../commands/dynamicHelp')
const SetGame = require('../commands/setGame')
const Prune = require('../commands/selfbotPrune')

/**
 * @type {Registry}
 */
const StandardRegistry = new Registry()
  .addCommand(Nickname)
  .addCommand(NameChange)
  .addCommand(Help)

/**
 * @type {Registry}
 */
const SelfbotRegistry = new Registry()
  .addCommand(Help)
  .addCommand(SetGame)
  .addCommand(Prune)

module.exports = { StandardRegistry, SelfbotRegistry }
