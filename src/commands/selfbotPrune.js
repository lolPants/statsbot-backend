const Command = require('../classes/Command')
const Backend = require('../index')
const MessageBuilder = require('../classes/MessageBuilder')

// JSDoc Typing
const Discord = require('discord.js')
const SelfbotClient = Backend.SelfbotClient // eslint-disable-line
const DiscordMessage = Discord.Message // eslint-disable-line

/**
 * @typedef {Object} CommandProperties
 * @property {SelfbotClient} client
 * @property {DiscordMessage} message
 * @property {Command} command
 * @property {string[]} arguments
 */

// Message Builder
let msgString = new MessageBuilder()
  .setName('Prune')

/**
 * @param {CommandProperties} data Command Properties
 */
const main = data => {
  try {
    if (data.arguments.length === 0) {
      // No Number Given
      data.client._sendErrorMessage(data.message, msgString.setMessage(
        'You didn\'t give a number of messages to prune!'
      ).setError().formattedMessage)
    } else {
      let num = parseInt(data.arguments[0])
      if (Number.isNaN(num)) {
        // Invalid Number
        data.client._sendErrorMessage(data.message, msgString.setMessage(
          'Invalid number!'
        ).setError().formattedMessage)
      } else {
        doPrune(data, num)
      }
    }
  } catch (err) {
    data.client._sendErrorMessage(data.message, msgString.setMessage(
      'Error Pruning Messages.'
    ).setError().formattedMessage)
  }
}

/**
 * @param {CommandProperties} data Command Data
 * @param {number} num Number of Messages
 */
const doPrune = async (data, num) => {
  let msg = await data.message.edit(msgString.setMessage(`Pruning ${num} message(s)...`).setError(false).setPending().formattedMessage)
  let messages = await data.message.channel.messages.fetch({ limit: 100 })
  messages = messages.filter(m => (m.author.id === data.client.user.id) && m.id !== data.message.id)
  let j = 0
  while (j < num) {
    var m = messages.array()[j]
    if ((j + 1) === num) m.delete().then(() => msg.delete())
    else m.delete()
    j++
  }
}

const Prune = new Command()
  .setName('prune')
  .setDescription('Deletes your previous messages.')
  .addArgument('number')
  .setMain(main)

module.exports = Prune
